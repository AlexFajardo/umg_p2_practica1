package practica1;

    import java.util.Scanner;

public class Practica1 {

    public static void main(String[] args) {
      Scanner entrada = new Scanner(System.in);
              
      System.out.println("Menú Principal");
      System.out.println("1.INGRESO Y TRATAMIENTO DE NOMBRES");
      System.out.println("2.CALCULO DE AREAS Y DIAMETROS");
      System.out.println("3.SALIR DEL SISTEMA");
      
      int Opcion = entrada.nextInt();
      
      String nombre1, nombre2, nombre3;
      String segundonom1,segundonom2,segundonom3;
      String apellido1,apellido2,apellido3;
      String segundoape1,segundoape2,segundoape3;
      
      int area,diametro,radesfera,larcubo,radcirculo,larcuadrado;
      
      double pi = 3.1416;
      
      if (Opcion == 1){
          
          System.out.println("Persona No1");
          
          System.out.println("Primer Nombre");
          nombre1 = entrada.next();
          
          System.out.println("Segundo Nombre");
          segundonom1 = entrada.next();
          
          System.out.println("Primer Apellido");
          apellido1 = entrada.next();
          
          System.out.println("Segundo Apellido");
          segundoape1 = entrada.next();
          
          System.out.println("Persona No2");
          
          System.out.println("Primer Nombre");
          nombre2 = entrada.next();
          
          System.out.println("Segundo Nombre");
          segundonom2 = entrada.next();
          
          System.out.println("Primer Apellido");
          apellido2 = entrada.next();
          
          System.out.println("Segundo Apellido");
          segundoape2 = entrada.next();
          
          System.out.println("Persona No3");
          
          System.out.println("Primer Nombre");
          nombre3 = entrada.next();
          
          System.out.println("Segundo Nombre");
          segundonom3 = entrada.next();
          
          System.out.println("Primer Apellido");
          apellido3 = entrada.next();
          
          System.out.println("Segundo Apellido");
          segundoape3 = entrada.next();
          
          System.out.println("1:" + apellido1 + "" + segundoape1 + "" + "," + "" + nombre1 + "" + segundonom1);
          System.out.println("2:" + apellido2 + "" + segundoape2 + "" + "," + "" + nombre2 + "" + segundonom2);
          System.out.println("3:" + apellido3 + "" + segundoape3 + "" + "," + "" + nombre3 + "" + segundonom2);
          
      } else if (Opcion ==2){
          
          System.out.println("1.Calculo de area");
          System.out.println("2.Calculo de diametro");
          
          int Opcion2 = entrada.nextInt();
          
          if (Opcion2 == 1){
              
              System.out.println("1. Esfera");
              System.out.println("2. Cubo");
              System.out.println("3. Circulo");
              System.out.println("4. Cuadrado");
              
              int Opcion3 = entrada.nextInt();
              
              if (Opcion3 == 1){
                  System.out.println("Ingrese el radio de la Esfera");
                  radesfera = entrada.nextInt();
                  
                  area = (int) (4* pi* radesfera * radesfera);
                  
                  System.out.println("El area es:" + area);
              }
              if (Opcion3 == 2) {
                  System.out.println("Ingrese el largo del Cubo");
                  larcubo = entrada.nextInt();
                  
                  area = 6 * larcubo * larcubo;
                  System.out.println("El area es: " + area);
                  
              }
              if(Opcion3 == 3){
                  System.out.println("Ingrese el radio del Circulo");
                  radcirculo = entrada.nextInt();
                  
                  area = (int) (pi * radcirculo * radcirculo);
                  System.out.println("El area es: " + area);
              }
              if(Opcion3 ==4){
                  System.out.println("Ingree el largo del Cuadrado");
                  larcuadrado = entrada.nextInt();
                  
                  area = larcuadrado * larcuadrado;
                  System.out.println("El area es: " + area);
             
              }
              
          } else if (Opcion2 == 2) {
              System.out.println("1. Esfera");
              System.out.println("2. Cubo");
              System.out.println("4. Cuadrado");
              
              int Opcion4 = entrada.nextInt();
              
              if (Opcion4 == 1){
                  System.out.println("Ingrese el radio de la Esfera");
                  radesfera = entrada.nextInt();
                  
                  diametro = (int) (1.3333* pi *(radesfera * radesfera * radesfera));
                  
                  System.out.println("El diametro es: " + diametro);
              }
              if (Opcion4 == 2){
                  System.out.println("Ingrese el largo del cubo");
                  larcubo = entrada.nextInt();
                  
                  diametro = larcubo * larcubo * larcubo;
                  
                  System.out.println("El diametro es:" + diametro);
              }
              if (Opcion4 == 3){
                  System.out.println("Ingrese el radio del Circulo");
                  radcirculo = entrada.nextInt();
                  
                  diametro = (int) (2 * pi * radcirculo);
                  
                  System.out.println("El diametro es: " + diametro);
              }
              if(Opcion4 ==4){
                  System.out.println("Ingrese el largo del Cuadrado");
                  larcuadrado = entrada.nextInt();
                  
                  diametro = larcuadrado * 4;
                  System.out.println("El diametro es:" + diametro);
                  
                
              }
          }
      }
      
    }
    
}